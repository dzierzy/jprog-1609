package pl.com.sages.jprog.zoo;


import java.util.StringTokenizer;

public abstract class Animal {

    private String name;

    private int size;

    public Animal(String name, int size) {
        this.name = name;
        this.size = size;
    }

    public abstract void move();

    public String getName() {

       /* StringBuilder builder = new StringBuilder();

        StringTokenizer tokenizer = new StringTokenizer(name);
        //String[] parts = name.split("\\s");
        //for(String part : parts){
        while(tokenizer.hasMoreTokens()){
            String part = tokenizer.nextToken();
            part = part.substring(0,1).toUpperCase() + part.substring(1);
            //name = name + " " + part;
            builder.append(part).append(" ");
        }
        return builder.reverse().toString().trim();*/
       return name;
    }

    public int getSize() {
        return size;
    }


    @Override
    public String toString() {
        return "Animal{" +
                "class=" + this.getClass().getName() +
                ", name='" + name + '\'' +
                ", size='" + size + '\'' +
                '}';
    }
}
