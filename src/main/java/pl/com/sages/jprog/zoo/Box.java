package pl.com.sages.jprog.zoo;

public class Box<T> {

    private T o;

    public T getO() {
        return o;
    }

    public void setO(T o) {
        this.o = o;
    }
}
