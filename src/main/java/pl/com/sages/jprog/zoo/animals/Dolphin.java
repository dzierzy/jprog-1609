package pl.com.sages.jprog.zoo.animals;

import pl.com.sages.jprog.zoo.Animal;

public class Dolphin extends Animal {

    public Dolphin(String name) {
        super(name, 300);
    }

    @Override
    public void move() {
        System.out.println("Dolphin is swimming!");
    }
}
