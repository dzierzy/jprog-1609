package pl.com.sages.jprog.zoo.animals;

import pl.com.sages.jprog.zoo.Animal;
import pl.com.sages.jprog.zoo.Dangerous;

@Dangerous(priority = 1)
public class Lion extends Animal {

    public Lion(String name) {
        super(name, 200);
    }

    @Override
    public void move() {
        System.out.println("Lion is running very fast!");
    }
}
