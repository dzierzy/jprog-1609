package pl.com.sages.jprog.zoo;

import java.util.function.Predicate;

public class AnimalNamePredicate implements Predicate<Animal> {

    @Override
    public boolean test(Animal animal) {
        return animal.getName().equals("Wally");
    }
}
