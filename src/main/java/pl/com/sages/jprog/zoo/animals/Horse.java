package pl.com.sages.jprog.zoo.animals;

import pl.com.sages.jprog.travel.ITransportation;
import pl.com.sages.jprog.travel.Person;
import pl.com.sages.jprog.zoo.Animal;

public class Horse extends Animal implements ITransportation {

    public Horse(String name) {
        super(name, 500);
    }

    @Override
    public void move() {
        System.out.println("Horse is running!");
    }

    @Override
    public void transport(Person passenger) {
        move();
        System.out.println("passenger " + passenger + " has been transported");
    }
}
