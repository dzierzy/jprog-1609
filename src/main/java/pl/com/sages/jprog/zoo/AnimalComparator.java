package pl.com.sages.jprog.zoo;

import java.util.Comparator;

public class AnimalComparator implements Comparator<Animal> {

    private boolean asc = true;

    public AnimalComparator(boolean asc) {
        this.asc = asc;
    }

    @Override
    public int compare(Animal o1, Animal o2) {
        int diff = asc ? o1.getSize() - o2.getSize() : o2.getSize() - o1.getSize();
        if(diff==0){
            diff = asc ? o1.getName().compareTo(o2.getName()) : o2.getName().compareTo(o1.getName());
        }
        return diff;
    }
}
