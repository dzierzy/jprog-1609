package pl.com.sages.jprog.zoo;

import pl.com.sages.jprog.zoo.animals.Dolphin;
import pl.com.sages.jprog.zoo.animals.Horse;
import pl.com.sages.jprog.zoo.animals.Lion;

import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ZooStarter{

    public static void main(String[] args) throws UnsupportedEncodingException {
        System.out.println("hey!");

        Locale.setDefault(Locale.CHINESE);

        Locale locale = new Locale("pl");
        ResourceBundle bundle = ResourceBundle.getBundle("pl.com.sages.jprog.zoo", locale);

        Animal a = new Dolphin(bundle.getString("animal.name"));
        a.move();
       
        String animalName = a.getName();

        /*byte[] byteArray = animalName.getBytes("ISO-8859-1");
        animalName = new String(byteArray, "ISO-8859-1");*/
        System.out.println("animalName = " + animalName);

        Comparator<Animal> comparator = new AnimalComparator(false);
        //List<Animal> animals = new ArrayList<>();
        Set<Animal> animals = new TreeSet<>(comparator);
        animals.add(a);
        animals.add(new Lion("Krol Lew"));
        animals.add(new Horse("Krol Lew"));
        animals.add(new Dolphin("Wally"));
        animals.add(a);

        //animals.removeIf(an -> an.getName().equals("Wally"));
        //animals.forEach(an -> System.out.println("animal: " + a));
        
        Stream<Animal> stream = animals.stream();
        //Set<String> animalNames 
        //boolean match 
        //long count 
        Optional<String> optionalName = stream
                .filter(an -> !an.getName().equals("Wally"))
                .sorted(new AnimalComparator(true))
                .map(an->an.getName())
                //.collect(Collectors.toSet());
                //.anyMatch(n->n.contains("a"));
                //.distinct()
                .limit(0)
                .findFirst();
                //.count();

        System.out.println("optionalName.get() = " + optionalName.orElseThrow(
                () -> new IllegalArgumentException("missing animal name")
        ));

        //System.out.println("count = " + count);
        //System.out.println("match = " + match);
        //System.out.println("animalNames = " + animalNames);
                



        System.out.println("animals count: " + animals.size());


        Box<Animal> box = new Box<>();
        box.setO(a);
        Animal animal = box.getO();
        System.out.println("animal from box: " + animal);

        Box<Box<Animal>> boxBox = new Box<>();
        boxBox.setO(box);

        System.out.println("done.");

    }


}
