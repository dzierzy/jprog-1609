package pl.com.sages.jprog.javafx.guess;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class GuessNumberApp extends Application {

    private TextField guessField;

    private SecretNumber secretNumber = new SecretNumber();

    private int counter;

    public void start(Stage primaryStage) throws Exception {

        primaryStage.setTitle("Guess My Number");

        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(25, 25, 25, 25));
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setHalignment(HPos.CENTER);
        pane.getColumnConstraints().add(column1);

        Scene scene = new Scene(pane, 400, 200);

        pane.add(new Text("There is a number to guess! It's bettwen 1 and 100."), 0, 0);

        Label guessLabel = new Label("Your guess: ");
        pane.add(guessLabel, 0, 1);

        guessField = new TextField();
        pane.add(guessField, 0, 2);

        // TODO on key pressed
        guessField.setOnKeyPressed(e->{
            if(e.getCode()== KeyCode.ENTER){
                handle(guessField.getText());
            }
        });

        Button button = new Button("Check !");
        pane.add(button, 0, 3);

        // TODO set button on action
        button.setOnAction(e->handle(guessField.getText()));


        primaryStage.setScene(scene);
        primaryStage.show();
    }


    private void handle(String value){
        counter++;
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Result");
        alert.setHeaderText("The result is ....");

        // TODO verify number and prepare message
        // TODO set content text

        int intValue = Integer.parseInt(value);
        int guess = secretNumber.guess(intValue);
        String message = guess==0 ? "Congratulations! Guessed after " + counter + " attempts. New secrete number" :
                guess<0 ? "Too big!" : "Too small";

        if(guess==0){
            counter = 0;
            secretNumber.reset();
        }

        alert.setContentText(message);
        alert.showAndWait();
    }

}
