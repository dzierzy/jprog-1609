package pl.com.sages.jprog.concurrency.account;

import java.util.concurrent.atomic.AtomicInteger;

public class Account {

    private AtomicInteger balance = new AtomicInteger(1_000_000);

    public boolean withdraw(int amount) {

        if (balance.get() >= amount) {
            balance.addAndGet(-amount);
            //balance -= amount; // 1. read current balance, 2. increment 3. assign
            return true;
        } else {
            return false;
        }
    }

    public void deposit(int amount) {
        balance.addAndGet(amount); // 1. read current balance, 2. increment 3. assign
    }

    public int getBalance() {
        return balance.get();
    }
}
