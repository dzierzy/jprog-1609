package pl.com.sages.jprog.concurrency.alarm;


public class Beeper implements Runnable{

    public void beep() {

        for (int i = 0; i < 20; i++) {
            System.out.println("beep");

            try {
                Thread.sleep(1 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void run(){
        beep();
    }
}
