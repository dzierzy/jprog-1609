package pl.com.sages.jprog.concurrency.alarm;


import pl.com.sages.jprog.concurrency.ThreadNamePrefixPrintStream;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Alarm {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));


        ExecutorService es = Executors.newFixedThreadPool(1);

        Beeper b = new Beeper();
        Light l = new Light();

        es.execute(b);
        es.execute(l);

        /*Thread t1 = new Thread(b);
        Thread t2 = new Thread(l);
        t1.start();
        t2.start();*/

        System.out.println("done.");


        es.shutdown();


    }
}
