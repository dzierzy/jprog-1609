package pl.com.sages.jprog.concurrency.account;

import pl.com.sages.jprog.concurrency.ThreadNamePrefixPrintStream;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadsStart {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        ExecutorService es = Executors.newFixedThreadPool(2);

        System.out.println("ThreadsStart.main");
        Account account = new Account();

        // TODO
        Runnable depositTask = () -> {
            for(int i=0; i<1_000_000; i++){
                account.deposit(1);
            }
            System.out.println("balance after deposit=" + account.getBalance());
        };

        Runnable withdrawTask = () -> {
            for(int i=0; i<1_000_000; i++){
                account.withdraw(1);
            }
            System.out.println("balance after withdraw=" + account.getBalance());
        };

        es.execute(depositTask);
        es.execute(withdrawTask);


        es.shutdown();
        System.out.println("done.");



    }

}
