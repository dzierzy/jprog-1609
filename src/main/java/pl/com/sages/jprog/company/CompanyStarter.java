package pl.com.sages.jprog.company;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CompanyStarter {


    // JPA -> Java Persistence API -> ORM  -> Hibernate
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        System.out.println("Let's check emplyees!");

        List<Company> companies = new ArrayList<>();
        Class.forName("org.h2.Driver");
        try(Connection c = DriverManager.getConnection("jdbc:h2:tcp://10.0.3.234/~/test", "sa", "")){
            PreparedStatement stmt = c.prepareStatement(
                    "select c.id as c_id, c.name, e.id as e_id, e.first_name, e.last_name from employee e join company c on c.id=e.company_id");
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                int companyId = rs.getInt("c_id");
                String companyName = rs.getString("name");
                Optional<Company> optionalCompany = companies.stream().filter(com->com.getId()==companyId).findFirst();
                Company com = null;
                if(optionalCompany.isPresent()){
                    com = optionalCompany.get();
                } else {
                    com = new Company(companyId, companyName);
                    companies.add(com);
                }
                Employee e = new Employee(
                        rs.getInt("e_id"),
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        com);
                com.getEmployees().add(e);
            }
        }

        System.out.println("companies: " + companies);

    }
}
