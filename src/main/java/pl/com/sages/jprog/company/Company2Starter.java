package pl.com.sages.jprog.company;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Company2Starter {


    // JPA -> Java Persistence API -> ORM  -> Hibernate
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        System.out.println("Let's check emplyees!");

        Class.forName("org.h2.Driver");
        try(Connection c = DriverManager.getConnection("jdbc:h2:tcp://10.0.3.234/~/test", "sa", "")){
            PreparedStatement stmt = c.prepareStatement(
                    "select c.id as c_id, c.name, e.id as e_id, e.first_name, e.last_name from employee e join company c on c.id=e.company_id");
            ResultSet rs = stmt.executeQuery();
            int columnCount = rs.getMetaData().getColumnCount();
            List<Object[]> allData = new ArrayList<>();
            while(rs.next()) {

                Object[] row = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    row[i - 1] = rs.getObject(i);
                }
                allData.add(row);
            }

            System.out.println("done.");
        }



    }
}
