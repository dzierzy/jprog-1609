package pl.com.sages.jprog.company;

import java.util.ArrayList;
import java.util.List;

public class Company {

    private int id;

    private String name;

    private List<Employee> employees = new ArrayList<>();

    public Company(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
