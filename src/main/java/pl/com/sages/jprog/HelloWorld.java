package pl.com.sages.jprog;

import pl.com.sages.jprog.calc.Calculator;

import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HelloWorld {

    //psvm
    public static void main(String[] args){
        System.out.println("Let's start!");


        long age = Long.MIN_VALUE; // autoboxing
        System.out.println("age=" + age);

        float price = 123.45678F;
        System.out.println("price = " + price); // soutv
        
        boolean cheap = price < 100;
        System.out.println("cheap = " + cheap);
        
        char c = '\r';
        System.out.println("c = " + (short)c);


        int[] dayOff = {5,7,9,17,27};

        System.out.println("dayOff = " + Arrays.toString(dayOff));
        System.out.println("dyOff array size=" + dayOff.length);

        for(int i : dayOff){
            System.out.println("i=" + i);
        }

        WeekDay weekDay = WeekDay.values()[2];
        System.out.println("weekDay = " + weekDay);
        
        boolean weekend = weekDay.ordinal()>4 ? true : false;
        System.out.println("weekend = " + weekend);

        switch (weekDay) {

            default:
                System.out.println("it's not weekend yet :(");
               // break;
            case SATURDAY: case SUNDAY:
                System.out.println("It's weekend :)");
                break;
        }

        int size;
        try {
            size = args.length > 0 ? Integer.parseInt(args[0]) : 10;
        } catch (NumberFormatException e){
            Scanner scanner = new Scanner(System.in);
            size = scanner.nextInt();
        }
        int[][] multiplicationTable = new int[size][size];

        boolean run = true;
        for(int x=1; x<=size && run; x++){
            for(int y=1; y<=size; y++){
                if(y==13){
                    run = false;
                    break;
                }
                int[] operands = {x,y};
                multiplicationTable[x-1][y-1] = Calculator.multiply(x,y); // var-args
            }
        }

        for( int[] row : multiplicationTable){
            for( int v : row){
                System.out.print(v + "\t");
            }
            System.out.println();
        }

        System.out.println("done");


        if(print(false) & print(true)){
            System.out.println("|| true");
        }


        String regEx = "(\\d{2})([-\\s]?)(\\d{3})";
        String text = "Address: 30-512 Krakow";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(text);
        while(m.find()){
            System.out.println("start:" + m.start() + " end: " + m.end() + ", el:" +m.group());
            int groupCount = m.groupCount();
            for(int i=0; i<=groupCount; i++){
                System.out.println("group(" + i + ")=" + m.group(i) );
            }
        }

    }

    public static boolean print(boolean b){
        System.out.println("b = " + b);
        return b;
    }

}
