package pl.com.sages.jprog.travel;

public class Bus implements ITransportation {

    @Override
    public void transport(Person passenger) {
        System.out.println("Bus is transporting passenger " + passenger);
    }

    @Override
    public int getSpeed() {
        return 40;
    }
}
