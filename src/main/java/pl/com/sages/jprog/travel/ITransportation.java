package pl.com.sages.jprog.travel;

@FunctionalInterface
public interface ITransportation {

    /*public abstract */void transport(Person passenger);

    default int getSpeed(){
        return 30;
    }

    static String getDescription(){
        return "Transportation";
    }

}
