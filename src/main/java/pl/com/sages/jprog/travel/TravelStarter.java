package pl.com.sages.jprog.travel;

import pl.com.sages.jprog.zoo.animals.Horse;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

public class TravelStarter {

    public static void main(String[] args) {
        System.out.println("Let's travel!");


        Comparator<Person> personComparator = (o1, o2) -> o1.getLastName().compareTo(o2.getLastName());

        /*        new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getLastName().compareTo(o2.getLastName());
            }
        };*/

        Map<Person, Integer> daysOff = new TreeMap<>(personComparator);
        daysOff.put(new Person(12345678, "Jan", "Kowalski"), 10);
        daysOff.put(new Person(12341234, "Adam", "Nowak"), 26);
        daysOff.put(new Person(89898989, "Krzysztof", "Kajak"), 0);

        System.out.println("daysOff registry size: " + daysOff.size());
        for( Map.Entry<Person, Integer> entry: daysOff.entrySet()){
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }

     /*   Person p1 = null;
        Person p2 = null;

        int h1 = p1.hashCode();
        int h2 = p2.hashCode();

        boolean same;
        if(h1==h2){
            same = p1.equals(p2);
        } else {
            same = false;
        }
*/
        Integer kowalskiDaysOff = daysOff.get(new Person(12345678, "Jan", "Kowalski"));

        if(kowalskiDaysOff > 7) {
            Horse h = new Horse("Kasztanka");
            ITransportation t =  p -> System.out.println("teleporting " + p);
                   /* new ITransportation() {
                @Override
                public void transport(Person passenger) {
                    System.out.println("teleporting " + passenger);
                }
            };*/
            System.out.println("transportation speed " + t.getSpeed());
            t.transport(new Person(12345678, "Jan", "Kowalski"));
        } else {
            System.out.println("Sorry Kowalski, go back to work!");
        }




    }
}
