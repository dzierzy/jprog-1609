package pl.com.sages.jprog.net;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    public static void main(String[] args) {
        System.out.println("Server.main");

        ExecutorService es = Executors.newFixedThreadPool(2);
        try {
            ServerSocket server = new ServerSocket(777);

            System.out.println("listening on port 1111...");
            Socket socket = server.accept();
            System.out.println("client connected.");

            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

            es.execute(new MessageReader(br));
            es.execute(new MessageWriter(bw));

        } catch (IOException e) {
            e.printStackTrace();
        }

        es.shutdown();
    }

}
