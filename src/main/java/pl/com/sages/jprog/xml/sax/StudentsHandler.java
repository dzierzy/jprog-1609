package pl.com.sages.jprog.xml.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class StudentsHandler extends DefaultHandler {

    boolean lastName = false;

    boolean trainer = false;

    @Override
    public void startElement(String uri,
                             String localName, String qName, Attributes attributes)
            throws SAXException {

        switch (qName) {
            case "lastName":
                lastName = true;
                break;
            case "trainer":
                trainer = true;
                break;
        }



    }

    @Override
    public void endElement(String uri,
                           String localName, String qName) throws SAXException {

        switch (qName) {
            case "lastName":
                lastName = false;
                break;
            case "trainer":
                trainer = false;
                break;
        }

    }

    @Override
    public void characters(char[] ch,
                           int start, int length) throws SAXException {

        if (lastName && !trainer) {
            String text = new String(ch, start, length);
            System.out.println("lastName: " + text);
        }
    }
}