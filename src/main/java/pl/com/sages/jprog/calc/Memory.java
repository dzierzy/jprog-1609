package pl.com.sages.jprog.calc;

public interface Memory {

    void writeValue(double v);

    double readValue();

    double[] getAllValues();

}
