package pl.com.sages.jprog.calc.impl;

import pl.com.sages.jprog.calc.Memory;

import java.util.Arrays;

public class ArrayMemory implements Memory {

    private double[] values = new double[5];

    private int index = -1;

    @Override
    public void writeValue(double v) {
        if(index<values.length-1) {
            index++;
        } else {
            System.arraycopy(values, 1, values, 0, values.length-1);
        }
        values[index] = v;
    }
    // [0,1,2,3,4] + 5 -> [1,2,3,4,5]

    @Override
    public double readValue() {
        return index>=0 ? values[index] : Double.NaN;
    }

    @Override
    public double[] getAllValues() {
        double[] subArray = new double[index+1];
        System.arraycopy(values, 0, subArray, 0, index+1);
        return subArray;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ArrayMemory{");
        sb.append("values=").append(Arrays.toString(values));
        sb.append(", index=").append(index);
        sb.append('}');
        return sb.toString();
    }
}
