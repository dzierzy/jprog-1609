package pl.com.sages.jprog.calc.impl;

import pl.com.sages.jprog.calc.Memory;

import java.util.ArrayList;
import java.util.List;

public class ListMemory implements Memory {

    private List<Double> list = new ArrayList<>();

    @Override
    public void writeValue(double v) {
        list.add(v);
    }

    @Override
    public double readValue() {
        return list.size()>0 ? list.get(list.size()-1) : Double.NaN;
    }

    @Override
    public double[] getAllValues() {
        double[] array = new double[list.size()];
        for(int i=0;i<list.size();i++){
            array[i] = list.get(i);
        }
        return array;
    }

    @Override
    public String toString() {
        return "ListMemory{" +
                "list=" + list +
                '}';
    }
}
