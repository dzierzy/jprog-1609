package pl.com.sages.jprog.calc;

import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class CalculatorStarter {


    public static void main(String[] args) {
        System.out.println("Let's count something!");

        try {
            AdvancedCalculator calc = new AdvancedCalculator();
            calc.add(2);
            calc.pow(-8);
            System.out.println("calc=" + calc.getResult());
        }catch (CalculatorException | IllegalArgumentException e){
            e.printStackTrace();
            return;
        }finally {
            System.out.println("in finally block");
        }

        Calculator calc2 = new Calculator( 2.0 );
        calc2.load();
        calc2.add(5);
        calc2.multiply(4);
        calc2.divide(1.7);
        //calc2.multiply(10000);
        System.out.println("calc before rotation=" + calc2);
        calc2.add(123);
        calc2.add(23);
        System.out.println("calc after rotation=" + calc2);


        Locale locale = new Locale("en", "PL");

      /*  Date date = new Date();
        DateFormat format = new SimpleDateFormat("YY/MM/dd hh*mm*ss*SS a G", locale);
                //DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.FULL, locale);*/
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YY/MM/dd hh*mm*ss*SS a G", locale);
                //DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        LocalDateTime localDate = LocalDateTime.now();
        String timestamp = formatter.format(localDate);

        double value = calc2.getResult();
        NumberFormat format = NumberFormat.getCurrencyInstance(locale);
        format.setMaximumFractionDigits(6);
        String valueString = format.format(value);
        System.out.println("[" + timestamp + "] calculator result=" + valueString);

        calc2.save();

        System.out.println("done.");

    }
}
