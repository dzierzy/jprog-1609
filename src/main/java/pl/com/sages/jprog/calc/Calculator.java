package pl.com.sages.jprog.calc;

import pl.com.sages.jprog.calc.impl.ListMemory;

import java.io.*;
import java.sql.*;

/**
 * The class responsisble for basic arithmetic operations. Maintains state.
 * \u2026
 */
public class Calculator {

    // FIELDS
    protected Memory memory = new ListMemory();

    // CONSTRUCTORS
    public Calculator(double result){
        this.memory.writeValue(result);
    }


    private File getFile(){
        File dir = new File("calc");
        if(!dir.exists()){
            System.out.println("creating directory " + dir.getAbsolutePath());
            dir.mkdirs();
        } else {
            System.out.println("directory " + dir.getAbsolutePath() + " already exists");
        }

        File file = new File(dir, "calc.memory");
        if(!file.exists()){
            System.out.println("creating file " + file.getAbsolutePath());
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("file " + file.getAbsolutePath() + " already exists");
        }
        return file;
    }

    private Connection getConnection(){
        String driverClassName = "org.h2.Driver";
        String url ="jdbc:h2:tcp://10.0.3.234/~/test";
        String user = "sa";
        String password = "";
        try {
            Class.forName(driverClassName);
            return DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    public void load(){

        try(Connection c = getConnection()){
            PreparedStatement stmt = c.prepareStatement("select value from memory order by id desc limit 1");
            ResultSet rs = stmt.executeQuery();
            if (rs.next()){
                double value = rs.getDouble("value");
                memory.writeValue(value);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public void save(){

        try(Connection c = getConnection()){
            c.setAutoCommit(false);
            try {
                PreparedStatement stmt = c.prepareStatement("insert into memory(value) values(?)");
                for (double value : memory.getAllValues()) {
                    stmt.setDouble(1, value);
                    stmt.addBatch();
                }
                stmt.executeBatch();
                c.commit();
            }catch (SQLException e){
                c.rollback();
                throw e;
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }

    }

    public void loadFromFile(){

        File file = getFile();


        try(BufferedReader br = new BufferedReader(new FileReader(file))){

            String line = null;
            String lastLine = null;
            while(  (line=br.readLine())!=null   ){
                System.out.println("line: " + line);
                lastLine = line;
            }
            if(lastLine!=null) {
                lastLine = lastLine.substring("value=".length());
                memory.writeValue(Double.parseDouble(lastLine));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * saves the latest value from memory
     */
    public void saveToFile(){

        // try-with-resources - >  AutoCloseable close()
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(getFile(), true))){
            bw.write("\nvalue=" + memory.readValue());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //BUSINESS INSTANCE METHODS
    public void add(double operand){
        this.memory.writeValue(memory.readValue()+operand);
    }

    public void subtract(double operand){
        this.memory.writeValue(memory.readValue()-operand);
    }

    /**
     * multiplies inner value with provided operand
     * @param operand multiplication operand
     */
    public void multiply(double operand){
        this.memory.writeValue(memory.readValue()*operand);
    }

    public void divide(double operand){
        this.memory.writeValue(memory.readValue()/operand);
    }

    // STATIC METHODS
    public static int multiply(int a, int... intArray){
        int result = a;
        for(int i : intArray){
            result*=i;
        }

        return result;
    }

    // TECHNICAL METHODS

    /**
     * Returns result
     * @return result
     */
    public double getResult() {
        return memory.readValue();
    }


    @Override
    public String toString() {
        return "Calculator{" +
                "memory=" + memory +
                '}';
    }
}
