package pl.com.sages.jprog.calc.impl;

import pl.com.sages.jprog.calc.Memory;

public class SimpleMemory implements Memory {

    private double value;

    @Override
    public void writeValue(double v) {
        value = v;
    }

    @Override
    public double readValue() {
        return value;
    }

    @Override
    public double[] getAllValues() {
        return new double[]{value};
    }
}
