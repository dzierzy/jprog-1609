package pl.com.sages.jprog.calc.impl;

import org.junit.Assert;
import org.junit.Test;

public class ListMemoryTest {

    @Test
    public void testIfNaN(){

        ListMemory listMemory = new ListMemory();
        listMemory.writeValue(2);
        double value = listMemory.readValue();
        Assert.assertEquals( Double.NaN, value,0.0 );

    }

}
